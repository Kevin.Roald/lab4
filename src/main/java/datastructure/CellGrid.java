package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] cells;

    public CellGrid(int rows, int columns, CellState initialState) {
        // TODO Auto-generated constructor stu
        this.rows = rows;
        this.cols = columns;
        this.cells = new CellState[rows][columns];
        for (int row = 0; row < this.rows; row++){
            for (int col = 0; col < cols; col++){
                cells[row][col] = initialState;
            }
    }


	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row < 0 || column < 0 || row >= this.rows || column >= this.cols)
            throw new IndexOutOfBoundsException();
        this.cells[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row < 0 || column < 0 || row >= this.rows || column >= this.cols)
            throw new IndexOutOfBoundsException();
        return this.cells[row][column];

    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid kopiVariabel = new CellGrid(this.rows, this.cols, null);
            for (int row = 0; row  < this.rows; row++) {
                for (int col = 0; col  < this.cols; col++) {
                    kopiVariabel.set(row, col, this.get(row, col));
                }
            }
        return kopiVariabel;
    }
    
}
